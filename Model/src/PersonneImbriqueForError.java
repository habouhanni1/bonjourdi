/**
 * Created by Rmeed on 23-Feb-18.
 */
public class PersonneImbriqueForError implements PersonneInterface {

    public PersonneImbriqueForError pe;

    public PersonneImbriqueForError getPe() {
        return pe;
    }

    public void setPe(PersonneImbriqueForError pe) {
        this.pe = pe;
    }


    @Override
    public void runMethode() {

    }


    @Override
    public void runWithSameTransact() {

    }

    @Override
    public void runWithNewTransact() {

    }
}
