/**
 * Created by Rmeed on 24-Feb-18.
 */
public interface JobInterface {

    public void runInTheSameTransact();

    public void runInNewTransact();

}
