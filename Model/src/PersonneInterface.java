/**
 * Created by Rmeed on 06-Jan-18.
 */
public interface PersonneInterface {

    void runMethode();

    void runWithSameTransact();

    void runWithNewTransact();
}
