/**
 * Created by Rmeed on 16-Jan-18.
 */

@Singleton
public class PersonneSingleton implements PersonneInterface {


    @Override
    public void runMethode() {

    }

    @Override
    public void runWithSameTransact() {

    }

    @Override
    public void runWithNewTransact() {

    }
}
