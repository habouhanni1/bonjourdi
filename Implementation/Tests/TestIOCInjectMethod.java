import org.junit.Assert;
import org.junit.Test;



class ClazzWithNoConstructor {
}

class ClazzWithNoParamConstructor {
    int a;
    int b;

    public ClazzWithNoParamConstructor(int a, int b){
    }

    public ClazzWithNoParamConstructor(){
    }
}


class ClazzWithNoNoParamConstructor {
    int a;
    int b;

    public ClazzWithNoNoParamConstructor(int a, int b){
    }

    public ClazzWithNoNoParamConstructor(int a){
    }
}



public class TestIOCInjectMethod {


    @Test(expected = InjectorException.class)
    public void testClassWithNoConstructor(){
        Assert.assertNotNull(IOC.getInstance(ClazzWithNoConstructor.class));
    }

    @Test()
    public void testClassWithNoParamConstr(){
        Assert.assertNotNull(IOC.getInstance(ClazzWithNoParamConstructor.class));
    }

    @Test(expected = InjectorException.class)
    public void testClassWithNoNoParamConstr(){
        Assert.assertNotNull(IOC.getInstance(ClazzWithNoNoParamConstructor.class));
    }

}
