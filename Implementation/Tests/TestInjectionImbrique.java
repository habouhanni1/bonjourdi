import org.junit.Assert;

import org.junit.Before;


import org.junit.Test;


public class TestInjectionImbrique {

    @Before
    public void init(){
        IOC.registerClass(this);
    }


    @Inject
    public PreferredInterface implementation;

    @Test
    public void testNotNull(){
        if(implementation instanceof PreferredClasse2){
            PreferredClasse2 p = (PreferredClasse2) implementation;
           // Assert.assertNotNull(p.implementation);
           // Assert.assertNotNull(p.preferredClasse);

            Assert.assertTrue(p.implementation instanceof InjectClass);
            Assert.assertTrue(p.preferredClasse instanceof PreferredClasse);

        }
        else{
            Assert.assertFalse(true);
        }
    }

    @Test(expected = InjectorException.class)
    public void testCycleDependecy(){
        IOC.getInstance(PersonneImbriqueForError.class);
    }
}
