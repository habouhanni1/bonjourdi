import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Rmeed on 06-Jan-18.
 */
public class TestPreferredAnnotation {

    @Before
    public void init(){
        IOC.registerClass(this);
    }

    @Inject
    public PreferredInterface implementation;




    @Test
    public void testType(){
        Assert.assertTrue(implementation instanceof PreferredClasse2);
    }

}
