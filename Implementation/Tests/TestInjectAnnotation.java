import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class TestInjectAnnotation {
    @Before
    public void init(){
        IOC.registerClass(this);
    }

    @Inject
    public InjectInterface implementation;

    @Test
    public void notNullTest(){
        Assert.assertNotNull(implementation);
    }

    @Test
    public void injectedType(){
        Assert.assertTrue(implementation instanceof InjectClass);
    }

}
