import org.reflections.Reflections;

import java.lang.reflect.Field;
import java.util.Set;


public class IOC {
    private  static Injector injector;

    static {
        injector = new Injector();
    }

    public static <T> T getInstance(Class<T> clazz){
        return injector.getInstance(clazz);
    }

    public static void registerClass(Object o){
        injector.registerClass(o);
    }
}
