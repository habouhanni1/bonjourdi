import org.reflections.Reflections;


import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;


public class Injector {
    private List<Class> requestedClasses = new ArrayList<>();
    private List<Class> instanciatedClasses = new ArrayList<>();

    //key : interface, value : implementation
    private Map<Class, Class> interfaceMappings = new HashMap<>();

    private Map<Class, Object> singletonInstances = new HashMap<>();


    void registerClass(Object o) {
        Class clazz = o.getClass();
        for (Field f : clazz.getFields()) {
            if (f.isAnnotationPresent(Inject.class)) {
                try {
                    f.set(o, getInstance(f.getType()));
                } catch (IllegalAccessException e) {
                    throw new InjectorException("Cannot inject Field " + f.getName() + " of " + o.getClass().getName());
                }
            }
        }
    }


    @SuppressWarnings("unchecked")
    public <T> T getInstance(Class<T> clazz) {

        Class<T> type = clazz;
        try {
            if (type.isInterface()) {
                Class<? extends T> implementation = getInterfaceImplementation(type);
                T instance = createNewInstance(implementation);
                return instance;
            }

            if(requestedClasses.contains(type)){
                if(!instanciatedClasses.contains(type)){
                    throw new InjectorException("Cyclic dependency");
                }
            }else{
                requestedClasses.add(type);
            }


            return createNewInstance(type);
        } catch (Exception e) {
            throw new InjectorException("Cannot Create Class hierarchy");
        }

    }



    @SuppressWarnings("unchecked")
    private <T> Class<? extends T> getInterfaceImplementation(Class<T> interfaze) {
        if (interfaceMappings.containsKey(interfaze)) {
            return interfaceMappings.get(interfaze);
        }

        Reflections reflection = new Reflections();
        Set<Class<? extends T>> implementations = reflection.getSubTypesOf(interfaze);
        if (implementations.size() == 0) {
            throw new InjectorException("Cannot find Implementation of " + interfaze.getName());
        }

        if (implementations.size() == 1) {
            //cannot access an element in a set without iter :(
            Class<? extends T> implementation = implementations.iterator().next();
            interfaceMappings.put(interfaze, implementation);
            return implementation;
        }

        boolean foundPreferred = false;
        Class<? extends T> preferred = null;
        for (Class<? extends T> implementation : implementations) {
            if (implementation.isAnnotationPresent(Preferred.class)) {
                if (foundPreferred) {
                    throw new InjectorException("Found multiple @Preferred for " + interfaze.getName());
                }
                preferred = implementation;
                foundPreferred = true;
            }
        }

        interfaceMappings.put(interfaze, preferred);
        return preferred;
    }


    @SuppressWarnings("unchecked")
    private <T> T createNewInstance(Class<T> clazz) {
        if(singletonInstances.containsKey(clazz)){
            return (T) singletonInstances.get(clazz);
        }


        final Constructor<T> constructor = findConstructor(clazz);
        try {
            T instance = constructor.newInstance();

            registerClass(instance);

            if(clazz.isAnnotationPresent(Singleton.class)){
                singletonInstances.put(clazz, instance);
            }

            instanciatedClasses.add(clazz);

            return instance;
        } catch (Exception e) {
            throw new InjectorException("An Exception was thrown while Creating instance of " + clazz.getName());
        }
    }

    @SuppressWarnings("unchecked")
    private <T> Constructor<T> findConstructor(Class<T> clazz) {
        Constructor<?>[] constructors = clazz.getConstructors();
        if (constructors.length == 0) {
            throw new InjectorException(clazz.getName() + " does not have a public constructor");
        }

        if (constructors.length > 0) {
            for (Constructor c : constructors) {
                if (c.getParameters().length == 0) {
                    // we can safely cast since they are no modification to return type of getConstructors.
                    return (Constructor<T>) c;
                }
            }
        }
        throw new InjectorException(clazz.getName() + " does not have a no param constrcutor");
    }
}
