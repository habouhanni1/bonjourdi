
public class InjectorException extends RuntimeException{

    InjectorException(String message){
        super(message);
    }
}
